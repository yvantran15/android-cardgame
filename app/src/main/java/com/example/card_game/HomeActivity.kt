package com.example.card_game

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val button = findViewById<Button>(R.id.validateButton)

        button.setOnClickListener {
            val firstPlayer = findViewById<EditText>(R.id.firstPlayer).text.toString()
            val secondPlayer = findViewById<EditText>(R.id.secondPlayer).text.toString()
            val nbOfCards = Integer.parseInt(findViewById<EditText>(R.id.nbOfCards).text.toString())
            if (firstPlayer.isEmpty() || secondPlayer.isEmpty()) {
                Toast.makeText(this, resources.getString(R.string.empty_name), Toast.LENGTH_SHORT).show()
            } else if (firstPlayer == secondPlayer) {
                Toast.makeText(this, resources.getString(R.string.same_names), Toast.LENGTH_SHORT).show()
            } else if (nbOfCards < 4 || nbOfCards > 52 || nbOfCards % 4 != 0) {
                Toast.makeText(this, resources.getString(R.string.incorrect_number), Toast.LENGTH_SHORT).show()
            }
            else {
                val intent = Intent(this, GameActivity::class.java)
                intent.putExtra("firstPlayer", firstPlayer).putExtra("secondPlayer", secondPlayer).putExtra("nbOfCards", nbOfCards)
                startActivityForResult(intent, 1)
            }
        }
    }

}