package com.example.card_game

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.os.Handler
import android.os.Looper


class GameActivity : AppCompatActivity() {

    private lateinit var recyclerView : RecyclerView
    private lateinit var cardAdapter : CardAdapter

    private lateinit var button : Button
    private val handler = Handler(Looper.getMainLooper())


    var runnable: Runnable = object : Runnable {
        override fun run() {
            button.isEnabled = cardAdapter.isValid()
            handler.postDelayed(this, 40)
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        val firstPlayerName = intent.getStringExtra("firstPlayer")
        val secondPlayerName = intent.getStringExtra("secondPlayer")
        val nbOfCards = intent.getIntExtra("nbOfCards", 8)

        recyclerView = findViewById(R.id.recyclerView)
        cardAdapter = CardAdapter(PlayingCard.pickCards(nbOfCards/4) as List<PlayingCard>, this)
        recyclerView.adapter = cardAdapter

        val firstPlayer = findViewById<TextView>(R.id.firstPlayerTV)
        val secondPlayer = findViewById<TextView>(R.id.secondPlayerTV)
        firstPlayer.text = firstPlayerName
        secondPlayer.text = secondPlayerName
        firstPlayer.setTextColor(resources.getColor(R.color.purple_200))

        button = findViewById(R.id.validateMoveButton)
        button.isEnabled = false

        updateLayoutManager(null)
        handler.post(runnable)
    }

    fun onValidateMove(view : View) {
        val firstPlayer = findViewById<TextView>(R.id.firstPlayerTV)
        val secondPlayer = findViewById<TextView>(R.id.secondPlayerTV)
        val tmp = firstPlayer.currentTextColor
        firstPlayer.setTextColor(secondPlayer.currentTextColor)
        secondPlayer.setTextColor(tmp)

        cardAdapter.moveItems()
        cardAdapter.clearSelection()
        disableButton()
    }

    private fun disableButton() {
        findViewById<Button>(R.id.validateMoveButton).isEnabled = false
    }

    private fun createLayoutManager(): RecyclerView.LayoutManager? {
        val span = 4
       return GridLayoutManager(this, span)
    }

    private fun updateLayoutManager(v: View?) {
        recyclerView.layoutManager = createLayoutManager()
    }


}