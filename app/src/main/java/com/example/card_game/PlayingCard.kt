package com.example.card_game

import android.os.Build
import androidx.annotation.RequiresApi
import java.util.*
import java.util.stream.Collectors
import java.util.stream.IntStream

enum class PlayingCard(val suit: CardSuit, val figure: CardFigure, val resource: Int) {
    H1(CardSuit.HEART, CardFigure.ACE, R.drawable.h1), H2(
        CardSuit.HEART,
        CardFigure.TWO,
        R.drawable.h2
    ),
    H3(CardSuit.HEART, CardFigure.THREE, R.drawable.h3), H4(
        CardSuit.HEART,
        CardFigure.FOUR,
        R.drawable.h4
    ),
    H5(CardSuit.HEART, CardFigure.FIVE, R.drawable.h5), H6(
        CardSuit.HEART,
        CardFigure.SIX,
        R.drawable.h6
    ),
    H7(CardSuit.HEART, CardFigure.SEVEN, R.drawable.h7), H8(
        CardSuit.HEART,
        CardFigure.EIGHT,
        R.drawable.h8
    ),
    H9(CardSuit.HEART, CardFigure.NINE, R.drawable.h9), H10(
        CardSuit.HEART,
        CardFigure.TEN,
        R.drawable.h10
    ),
    HJ(CardSuit.HEART, CardFigure.JACK, R.drawable.hj), HQ(
        CardSuit.HEART,
        CardFigure.QUEEN,
        R.drawable.hq
    ),
    HK(CardSuit.HEART, CardFigure.KING, R.drawable.hk), D1(
        CardSuit.DIAMOND,
        CardFigure.ACE,
        R.drawable.d1
    ),
    D2(CardSuit.DIAMOND, CardFigure.TWO, R.drawable.d2), D3(
        CardSuit.DIAMOND,
        CardFigure.THREE,
        R.drawable.d3
    ),
    D4(CardSuit.DIAMOND, CardFigure.FOUR, R.drawable.d4), D5(
        CardSuit.DIAMOND,
        CardFigure.FIVE,
        R.drawable.d5
    ),
    D6(CardSuit.DIAMOND, CardFigure.SIX, R.drawable.d6), D7(
        CardSuit.DIAMOND,
        CardFigure.SEVEN,
        R.drawable.d7
    ),
    D8(CardSuit.DIAMOND, CardFigure.EIGHT, R.drawable.d8), D9(
        CardSuit.DIAMOND,
        CardFigure.NINE,
        R.drawable.d9
    ),
    D10(CardSuit.DIAMOND, CardFigure.TEN, R.drawable.d10), DJ(
        CardSuit.DIAMOND,
        CardFigure.JACK,
        R.drawable.dj
    ),
    DQ(CardSuit.DIAMOND, CardFigure.QUEEN, R.drawable.dq), DK(
        CardSuit.DIAMOND,
        CardFigure.KING,
        R.drawable.dk
    ),
    C1(CardSuit.CLUB, CardFigure.ACE, R.drawable.c1), C2(
        CardSuit.CLUB,
        CardFigure.TWO,
        R.drawable.c2
    ),
    C3(CardSuit.CLUB, CardFigure.THREE, R.drawable.c3), C4(
        CardSuit.CLUB,
        CardFigure.FOUR,
        R.drawable.c4
    ),
    C5(CardSuit.CLUB, CardFigure.FIVE, R.drawable.c5), C6(
        CardSuit.CLUB,
        CardFigure.SIX,
        R.drawable.c6
    ),
    C7(CardSuit.CLUB, CardFigure.SEVEN, R.drawable.c7), C8(
        CardSuit.CLUB,
        CardFigure.EIGHT,
        R.drawable.c8
    ),
    C9(CardSuit.CLUB, CardFigure.NINE, R.drawable.c9), C10(
        CardSuit.CLUB,
        CardFigure.TEN,
        R.drawable.c10
    ),
    CJ(CardSuit.CLUB, CardFigure.JACK, R.drawable.cj), CQ(
        CardSuit.CLUB,
        CardFigure.QUEEN,
        R.drawable.cq
    ),
    CK(CardSuit.CLUB, CardFigure.KING, R.drawable.ck), S1(
        CardSuit.SPADE,
        CardFigure.ACE,
        R.drawable.s1
    ),
    S2(CardSuit.SPADE, CardFigure.TWO, R.drawable.s2), S3(
        CardSuit.SPADE,
        CardFigure.THREE,
        R.drawable.s3
    ),
    S4(CardSuit.SPADE, CardFigure.FOUR, R.drawable.s4), S5(
        CardSuit.SPADE,
        CardFigure.FIVE,
        R.drawable.s5
    ),
    S6(CardSuit.SPADE, CardFigure.SIX, R.drawable.s6), S7(
        CardSuit.SPADE,
        CardFigure.SEVEN,
        R.drawable.s7
    ),
    S8(CardSuit.SPADE, CardFigure.EIGHT, R.drawable.s8), S9(
        CardSuit.SPADE,
        CardFigure.NINE,
        R.drawable.s9
    ),
    S10(CardSuit.SPADE, CardFigure.TEN, R.drawable.s10), SJ(
        CardSuit.SPADE,
        CardFigure.JACK,
        R.drawable.sj
    ),
    SQ(CardSuit.SPADE, CardFigure.QUEEN, R.drawable.sq), SK(
        CardSuit.SPADE,
        CardFigure.KING,
        R.drawable.sk
    );


    companion object {
        /**
         * Pick groups of cards (one group contains a card figure for the 4 suits)
         * Return groupNumber * 4 cards
         *
         * @param groupNumber the number of groups to pick
         */
        @RequiresApi(Build.VERSION_CODES.N)
        fun pickCards(groupNumber: Int): List<PlayingCard?> {
            require(!(groupNumber < 0 || groupNumber > 13)) { "Number of groups must be between 0 and 13" }
            val cards = Arrays.asList(*values())
            // sort cards by figure and suit
            Collections.sort(cards) { x: PlayingCard, y: PlayingCard ->
                val cmp = x.figure.compareTo(y.figure)
                if (cmp != 0) return@sort cmp
                x.suit.compareTo(y.suit)
            }
            val l = IntStream.range(0, 13).mapToObj { x: Int -> x }
                .collect(Collectors.toList())
            Collections.shuffle(l)
            val result: MutableList<PlayingCard?> = ArrayList()
            l.stream().limit(groupNumber.toLong()).forEach { x: Int? ->
                result.addAll(
                    cards.subList(4 * x!!, 4 * (x + 1))
                )
            }
            Collections.shuffle(result)
            return result
        }
    }
}