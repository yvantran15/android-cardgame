# Android CardGame

# Principe du jeu

La partie débute en disposant les cartes, faces visibles sur le plateau de jeu. Les cartes forment des piles : initialement les cartes sont disposées en piles ne comportant qu'une seule carte.

A chaque tour de jeu, le joueur choisit une pile A (source) et une autre pile B (target) et il place la pile A sur la pile B (les deux piles sont donc fusionnées en une seule pile). Pour avoir le droit de fusionner les deux piles, au moins l'une de ces deux règles doit être respectée :

- soit les deux piles A et B comportent le même nombre de cartes (nombre indiqué dans le coin supérieur droit);

- soit les cartes figurant au dessus des piles A et B partagent la même enseigne.
Lorsqu'un joueur ne peut plus réaliser de fusion de piles (on ne trouve plus deux piles avec le même nombre de cartes ou la même enseigne sur le dessus), le jeu est terminé et ce joueur a perdu.
