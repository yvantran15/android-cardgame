package com.example.card_game

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


class CardAdapter(cards: List<PlayingCard>, private val context : Context) :
                        RecyclerView.Adapter<CardAdapter.ViewHolder>() {

    private val stacks = ArrayList<ArrayList<PlayingCard>>()
    private var source = -1
    private var target = -1

    private lateinit var sourceHolder : ViewHolder
    private lateinit var targetHolder : ViewHolder

    private var isValid : Boolean = false

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val imageView: ImageView = itemView.findViewById(R.id.cardImage)
        private val stackSizeTV: TextView = itemView.findViewById(R.id.stackSizeTV)
        val selectedTV : TextView = itemView.findViewById(R.id.selectedTV)

        fun update(stack : List<PlayingCard>) {
            imageView.setImageResource(stack[stack.size - 1].resource)
            stackSizeTV.text = stack.size.toString()
        }

    }

    init {
        for (i in cards.indices) {
            val stack = ArrayList<PlayingCard>()
            stack.add(cards[i])
            stacks.add(stack)
        }
    }


    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.card, viewGroup, false)
        )
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        viewHolder.update(stacks[viewHolder.adapterPosition])
        viewHolder.imageView.setOnClickListener {

            if (viewHolder.adapterPosition == source) {
                source = -1
                viewHolder.selectedTV.text = ""
            } else if (viewHolder.adapterPosition == target) {
                target = -1
                viewHolder.selectedTV.text = ""
            } else if (source == -1) {
                source = viewHolder.adapterPosition
                viewHolder.selectedTV.text = context.resources.getString(R.string.source)
                sourceHolder = viewHolder
            } else if (target == -1) {
                if (viewHolder.adapterPosition != source) {
                    target = viewHolder.adapterPosition
                    viewHolder.selectedTV.text = context.resources.getString(R.string.target)
                    targetHolder = viewHolder
                }
            }

            validate()

        }
    }

    private fun validate() {
        if (source == -1 || target == -1 ){
            return
        }
        isValid = stacks[source].size == stacks[target].size ||
                stacks[source][stacks[source].size-1].suit == stacks[target][stacks[target].size-1].suit
    }

    fun isValid() : Boolean {
        return isValid
    }

    fun clearSelection(){
        sourceHolder.selectedTV.text = ""
        targetHolder.selectedTV.text = ""
        source = -1
        target = -1
        isValid = false
    }

    fun moveItems() {
        stacks[target].addAll(stacks[source])
        stacks.removeAt(source)
        notifyItemChanged(target)
        notifyItemRemoved(source)
    }

    override fun getItemCount(): Int {
        return stacks.size
    }
}