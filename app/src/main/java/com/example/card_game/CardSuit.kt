package com.example.card_game

enum class CardSuit {
    HEART,
    DIAMOND,
    CLUB,
    SPADE
}